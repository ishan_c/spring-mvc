<%--
  Created by IntelliJ IDEA.
  User: ishan_w
  Date: 10/5/2018
  Time: 9:36 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Employee Registration Form</title>

    <style>

        .error {
            color: #ff0000;
        }
    </style>

</head>

<body>

<h2>Log In</h2>

<form:form method="POST" modelAttribute="loginEntity" action="login">
    Message is: ${name}
    <table>
        <tr>
            <td><label for="username">username: </label> </td>
            <td><form:input path="username" /></td>
        </tr>
        <tr>
            <td><label for="password">password: </label> </td>
            <td><form:input path="password"  /></td>
        </tr>
        <input type="submit" value="login"/>
    </table>
</form:form>
<br/>

</body>
</html>