package com.websystique.springmvc.bean;

import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by ishan_w on 10/11/2018.
 */
public class Emp {
    private String id;
    private String name;
    private LocalDate joiningDate;
    private BigDecimal salary;
    private String aaData;
    private int iTotalRecords;
    private int iTotalDisplayRecords;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(LocalDate joiningDate) {
        this.joiningDate = joiningDate;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }



    public int getiTotalRecords() {
        return iTotalRecords;
    }

    public void setiTotalRecords(int iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public int getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public String getAaData() {
        return aaData;
    }

    public void setAaData(String aaData) {
        this.aaData = aaData;
    }
}
