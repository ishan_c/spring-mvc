package com.websystique.springmvc.service;

import java.util.List;

import com.websystique.springmvc.bean.Emp;
import com.websystique.springmvc.model.Employee;
import com.websystique.springmvc.model.Login;

public interface EmployeeService {

	Employee findById(int id);
	
	void saveEmployee(Employee employee);
	
	void updateEmployee(Employee employee);
	
	void deleteEmployeeBySsn(String ssn);

	List<Emp> findAllEmployees();
	
	Employee findEmployeeBySsn(String ssn);

	boolean isEmployeeSsnUnique(Integer id, String ssn);

	Login check(String username, String password);
	
}
