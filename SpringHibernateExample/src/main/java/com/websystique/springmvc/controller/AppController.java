package com.websystique.springmvc.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.websystique.springmvc.bean.Emp;
import org.springframework.web.bind.annotation.ResponseBody;
import com.websystique.springmvc.Entitiy.LoginEntity;
import com.websystique.springmvc.model.Login;
import org.hibernate.Session;
import org.hibernate.type.SpecialOneToOneType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.websystique.springmvc.model.Employee;
import com.websystique.springmvc.Entitiy.LoginEntity;
import com.websystique.springmvc.service.EmployeeService;
import com.google.gson.Gson;

@Controller
@RequestMapping("/")
public class AppController {

	@Autowired
	EmployeeService service;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	MessageSource messageSource;
	/*
	 * This method will list all existing employees.
	 */
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String listEmployees(ModelMap model) {

//		List<Employee> employees = service.findAllEmployees();
		LoginEntity loginEntity=new LoginEntity();
		model.addAttribute("loginEntity", loginEntity);
		model.addAttribute("name","geeth");
//		return "allemployees";
		return "login";
	}



	/*
	 * This method will provide the medium to add a new Emp.
	 */
	@RequestMapping(value = { "/new" }, method = RequestMethod.GET)
	public String newEmployee(ModelMap model) {
		Employee employee = new Employee();
		model.addAttribute("employee", employee);
		model.addAttribute("edit", false);
		return "registration";
	}

	/*
	 * This method will be called on form submission, handling POST request for
	 * saving Emp in database. It also validates the user input
	 */
	@RequestMapping(value = { "/new" }, method = RequestMethod.POST)
	public String saveEmployee(@Valid Employee employee,
			ModelMap model) {



		/*
		 * Preferred way to achieve uniqueness of field [ssn] should be implementing custom @Unique annotation
		 * and applying it on field [ssn] of Model class [Employee].
		 *
		 * Below mentioned peace of code [if block] is to demonstrate that you can fill custom errors outside the validation
		 * framework as well while still using internationalized messages.
		 *
		 */
		if(!service.isEmployeeSsnUnique(employee.getId(), employee.getSsn())){
			FieldError ssnError =new FieldError("Emp","ssn",messageSource.getMessage("non.unique.ssn", new String[]{employee.getSsn()}, Locale.getDefault()));

			return "registration";
		}

		service.saveEmployee(employee);

		model.addAttribute("success", "Employee " + employee.getName() + " registered successfully");
		return "success";
	}


	/*
	 * This method will provide the medium to update an existing Emp.
	 */
	@RequestMapping(value = { "/edit-{ssn}-employee" }, method = RequestMethod.GET)
	public String editEmployee(@PathVariable String ssn, ModelMap model) {
		Employee employee = service.findEmployeeBySsn(ssn);
		model.addAttribute("employee", employee);
		model.addAttribute("edit", true);
		return "registration";
	}

	/*
	 * This method will be called on form submission, handling POST request for
	 * updating Emp in database. It also validates the user input
	 */
	@RequestMapping(value = { "/edit-{ssn}-employee" }, method = RequestMethod.POST)
	public String updateEmployee(@Valid Employee employee, BindingResult result,
			ModelMap model, @PathVariable String ssn) {

		if (result.hasErrors()) {
			return "registration";
		}

		if(!service.isEmployeeSsnUnique(employee.getId(), employee.getSsn())){
			FieldError ssnError =new FieldError("Emp","ssn",messageSource.getMessage("non.unique.ssn", new String[]{employee.getSsn()}, Locale.getDefault()));
		    result.addError(ssnError);
			return "registration";
		}

		service.updateEmployee(employee);

		model.addAttribute("success", "Employee " + employee.getName()	+ " updated successfully");
		return "success";
	}


	/*
	 * This method will delete an Emp by it's SSN value.
	 */
	@RequestMapping(value = { "/delete-{ssn}-employee" }, method = RequestMethod.GET)
	public String deleteEmployee(@PathVariable String ssn) {
		service.deleteEmployeeBySsn(ssn);
		return "redirect:/list";
	}

	@RequestMapping(value = { "/list" }, method = RequestMethod.GET)
	public @ResponseBody String listEmployee(ModelMap model) {
		System.out.println("lllllllllllllooooooooooooooogggggggggggggggginn");
//		if (httpSession.getAttribute("username") == null) {
//			LoginEntity loginEntity=new LoginEntity();
//			model.addAttribute("loginEntity", loginEntity);
//			return "login";
//		} else {
		Emp emp=new Emp();
			List<Emp> employees = service.findAllEmployees();

			Gson gson = new Gson();
			String aaData = gson.toJson(employees);
////			model.addAttribute("aaData",aaData);
//			emp.setAaData(aaData);
//			emp.setiTotalRecords(employees.size());
//			emp.setiTotalDisplayRecords(employees.size());
//			model.addAttribute("employee", emp);
			return aaData;
			//AddOnCardJsonObject addOnCardJsonObject = new AddOnCardJsonObject()

		//}
	}

	@RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
	public String logout(ModelMap model) {
		httpSession.invalidate();
		LoginEntity loginEntity=new LoginEntity();
		model.addAttribute("loginEntity", loginEntity);
		return "login";
		}



	@RequestMapping(value = { "/login" }, method = RequestMethod.POST)
	public String login(@ModelAttribute("loginEntity") LoginEntity loginEntity) {
		//System.out.println("lllllllllllllooooooooooooooogggggggggggggggginn");
//		List<Employee> employees = service.findAllEmployees();
//		model.addAttribute("employees", employees);
//		return "allemployees";
		String name=loginEntity.getUsername();
		String password=loginEntity.getPassword();
		Login user = service.check(name,password);

		if(user == null){
			return "login";

		}else{
			httpSession.setAttribute("username", name);
//			return "redirect:/list";
			return "allemployees";
		}


	}

}
