package com.websystique.springmvc.dao;

import java.util.List;

import com.websystique.springmvc.bean.Emp;
import com.websystique.springmvc.model.Employee;
import com.websystique.springmvc.model.Login;

public interface EmployeeDao {

	Employee findById(int id);

	void saveEmployee(Employee employee);
	
	void deleteEmployeeBySsn(String ssn);
	
	List<Emp> findAllEmployees();

	Employee findEmployeeBySsn(String ssn);

	Login findUser(String username, String password);
}
