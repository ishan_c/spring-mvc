package com.websystique.springmvc.dao;

import java.util.List;

import com.websystique.springmvc.Entitiy.LoginEntity;
import com.websystique.springmvc.bean.Emp;
import com.websystique.springmvc.model.Login;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.websystique.springmvc.model.Employee;

@Repository("employeeDao")
public class EmployeeDaoImpl extends AbstractDao<Integer, Employee> implements EmployeeDao {

	public Employee findById(int id) {
		return getByKey(id);
	}

	public void saveEmployee(Employee employee) {
		save(employee);
	}


	public void deleteEmployeeBySsn(String ssn) {
		Query query = getSession().createSQLQuery("delete from Employee where ssn = :ssn");
		query.setString("ssn", ssn);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<Emp> findAllEmployees() {
		Criteria criteria = createEntityCriteria();
		return (List<Emp>) criteria.list();
	}

	public Employee findEmployeeBySsn(String ssn) {

		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("ssn", ssn));
		return (Employee) criteria.uniqueResult();
	}

	public Login findUser(String username, String password){
		Login login=null;
		//System.out.println("eeeeeeeeeeeeeeeeeeeeeee "+ username + password);
		Query query = getSession().createSQLQuery("select * from Login where username= :username AND password= :password");

		query.setString("username", username);
		query.setString("password", password);
		try {
			login = (Login) query.setResultTransformer(Transformers.aliasToBean(Login.class)).list().get(0);
			//Login login = (Login) query.list().get(0);
		}catch (IndexOutOfBoundsException ae){

		}
			return login;
		}


}
