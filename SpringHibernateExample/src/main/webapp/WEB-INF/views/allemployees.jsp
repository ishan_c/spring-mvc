
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	  xmlns:th="http://www.thymeleaf.org">
<head>
	<meta charset="utf-8" />
	<title>Spring Boot + JPA + Datatables</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="/js/datatable.js"></script>
	<script>
	$(document).ready( function () {
	var table = $('#employeesTable?rows=2').DataTable({
	"sAjaxSource": "/list",
	"sAjaxDataProp": "",
	"order": [[ 0, "asc" ]],
	"aoColumns": [
        {	"title": "Id",
            "mData": "id",
            "targets": 0
        },
        {	"title": "name",
            "mData": "name",
            "targets": 1
        },
        {	"title": "joiningDate",
            "mData": "joiningDate",
            "targets": 2
        },
        {	"title": "salary",
            "mData": "salary",
            "targets": 3
        },

	]
	})
	});
	</script>
</head>

<body>
<h1>Employees Table</h1>
<div class="ui-widget-header ui-corner-top dtable-header">
	<span class="ui-icon ui-icon-circle-triangle-n"></span><span >Merchant Terminal</span>
</div>
<table id="employeesTable" class="cell-border display compact dtable" cellspacing="0" width="98%">
	<thead class="dthead"></thead>
	<tbody>
	</tbody>
</table>

</body>
</html>
